# OpenML dataset: Minneapolis-Air-Quality-Survey

https://www.openml.org/d/43747

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Minneapolis air quality survey results 
Content
Contained in the file are Minneapolis air quality survey results obtained between November 2013 and August 2014. The data set was obtained from http://opendata.minneapolismn.gov.
Inspiration
Visualizing air pollutants quantities over the city of Minneapolis may provide evidence for the source of certain air pollutants.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43747) of an [OpenML dataset](https://www.openml.org/d/43747). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43747/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43747/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43747/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

